const { ipcRenderer } = require('electron');


ipcRenderer.invoke('getCounter')
  .then((data) => {
    document.getElementById('score').textContent = data;
  })
  .catch((err) => {
    console.log(err);
  });

ipcRenderer.invoke('getMaxScore')
  .then((data) => {
    document.getElementById('maxScore').textContent = data;
  })
  .catch((err) => {
    console.log(err);
  });

  ipcRenderer.invoke('getVisitors')
  .then((data) => {
    const visitorNamesElement = document.getElementById('visitors');
    visitorNamesElement.innerHTML = ''; 
    data.forEach(visitor => {
      const p = document.createElement('p');
      p.innerText = visitor.visitor;
      visitorNamesElement.appendChild(p);
    });
  })
  .catch((err) => {
    console.log(err);
  });


const refreshCounters = () => {
ipcRenderer.invoke('getCounter')
    .then((data) => {
    document.getElementById('score').textContent = data;
    })
    .catch((err) => {
    console.log(err);
    });

    ipcRenderer.invoke('getMaxScore')
  .then((data) => {
    document.getElementById('maxScore').textContent = data;
  })
  .catch((err) => {
    console.log(err);
  });
};

const refreshVisitorList = () => {
    ipcRenderer.invoke('getVisitors')
      .then((data) => {
        const visitorNamesElement = document.getElementById('visitors');
        visitorNamesElement.innerHTML = '';
        data.forEach(visitor => {
          const p = document.createElement('p');
          p.innerText = visitor.visitor;
          visitorNamesElement.appendChild(p);
        });
      })
      .catch((err) => {
        console.log(err);
      });
  };


document.addEventListener('DOMContentLoaded', () => {

const add1Button = document.getElementById('add1Button');
add1Button.addEventListener('click', () => {
  add1ToCounter();
});
const add1ToCounter = async () => {
  try {
    await fetch('http://localhost:8080/add1/count', {
      method: 'PUT'
    });
    refreshCounters();
  } catch (error) {
    console.log(error);
  }
};


const add10Button = document.getElementById('add10Button');
add10Button.addEventListener('click', () => {
  add10ToCounter();
});
const add10ToCounter = async () => {
  try {
    await fetch('http://localhost:8080/add10/count', {
      method: 'PUT'
    });
    refreshCounters();
  } catch (error) {
    console.log(error);
  }
};


const subtract1Button = document.getElementById('Subtract1Button');
subtract1Button.addEventListener('click', () => {
  subtract1ToCounter();
});
const subtract1ToCounter = async () => {
  try {
    await fetch('http://localhost:8080/subtract1/count', {
      method: 'PUT'
    });
    refreshCounters();
  } catch (error) {
    console.log(error);
  }
};


const subtract10Button = document.getElementById('Subtract10Button');
subtract10Button.addEventListener('click', () => {
  subtract10ToCounter();
});
const subtract10ToCounter = async () => {
  try {
    await fetch('http://localhost:8080/subtract10/count', {
      method: 'PUT'
    });
    refreshCounters();
  } catch (error) {
    console.log(error);
  }
};


const restartCounter = document.getElementById('ResetButton');
restartCounter.addEventListener('click', () => {
  resetCounter();
});
const resetCounter = async () => {
  try {
    await fetch('http://localhost:8080/counter', {
      method: 'DELETE'
    });
    refreshCounters();
  } catch (error) {
    console.log(error);
  }
};

});

const visitorNameInput = document.getElementById('visitorNameInput');

visitorNameInput.addEventListener('keydown', (event) => {
  if (event.key === 'Enter') {
    const visitorName = visitorNameInput.value.trim();

    if (visitorName !== '') {
      addVisitor(visitorName);
      visitorNameInput.value = '';
    }
  }
});

const addVisitor = async (visitorName) => {
    try {
      const response = await fetch('http://localhost:8080/visitors', {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json'
        },
        body: JSON.stringify({ visitor: visitorName })
      });
      const newVisitor = await response.json();
      refreshVisitorList();
    } catch (error) {
      console.log(error);
    }
  };

setInterval(refreshCounters, 1000);
setInterval(refreshVisitorList, 5000);



