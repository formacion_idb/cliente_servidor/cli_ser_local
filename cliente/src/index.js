const {createWindow} = require('./main')
const {ipcMain} = require('electron')

const {app} = require('electron')
app.disableHardwareAcceleration();

app.whenReady().then(createWindow)


app.whenReady().then(() => { 
    ipcMain.handle('getCounter', async (event) => {
      try {
        const response = await fetch('http://127.0.0.1:8080/counter');
        const data = await response.json();
        return data;
      } catch (error) {
        throw error;
      }
    });
    ipcMain.handle('getMaxScore',async (event) => {
        try{
            const response = await fetch('http://localhost:8080/maxCounter');
            const data = await response.json();
            return data;
        }catch (error) {
            throw error;
        }
    });
    ipcMain.handle('getVisitors', async (event) =>{
        try {
            const response = await fetch('http://127.0.0.1:8080/visitors');
            const data = await response.json();
            data => {
                visitorNamesElement.innerHTML = '';
                data.forEach(visitor => {
                  const p = document.createElement('p');
                  p.innerText = visitor.visitor;
                  visitorNamesElement.appendChild(p);
                });
              }
            return data;
          } catch (error) {
            throw error;
          }
    });
});
