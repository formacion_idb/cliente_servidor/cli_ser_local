const http = require('http');

const hostname = '127.0.0.1';
const port = 8080;

let visitors = [
{ id: 1, visitor: 'Daniel'}
];

let counter = 0;
let maxCount = 0;

const server = http.createServer((req, res) => {
    if(req.url == "/counter" && req.method == "GET") {
      res.statusCode = 200;
      res.setHeader('Content-type', 'application/json');
      res.end(JSON.stringify(counter));
    } else if (req.url == "/counter" && req.method == "DELETE") {
      counter = 0;
      res.statusCode = 200;
      res.setHeader('Content-type', 'application/json');
      res.end(JSON.stringify(counter));
    } else if(req.url=="/add1/count" && req.method == "PUT") {
      counter++;
      if (counter > maxCount){
        maxCount = counter;
      }
      res.statusCode = 200;
      res.setHeader('Content-type', 'application/json');
      res.end(JSON.stringify(counter));
    } else if(req.url=="/add10/count" && req.method == "PUT") {
      counter += 10;
      if (counter > maxCount){
        maxCount = counter;
      }
      res.statusCode = 200;
      res.setHeader('Content-type', 'application/json');
      res.end(JSON.stringify(counter));
    } else if(req.url=="/subtract1/count" && req.method == "PUT") {
      counter--;
      res.statusCode = 200;
      res.setHeader('Content-type', 'application/json');
      res.end(JSON.stringify(counter));
    } else if(req.url=="/subtract10/count" && req.method == "PUT") {
      counter -= 10;
      res.statusCode = 200;
      res.setHeader('Content-type', 'application/json');
      res.end(JSON.stringify(counter));
    } else if(req.url=="/maxCounter" && req.method == "GET") {
        res.statusCode = 200;
        res.setHeader('Content-type', 'application/json');
        res.end(JSON.stringify(maxCount));
    } else if(req.url=="/visitors" && req.method == "GET") {
      res.statusCode = 200;
      res.setHeader('Content-type', 'application/json');
      res.end(JSON.stringify(visitors));
    } else if(req.url=="/visitors" && req.method == "POST") {
      let body = '';
      req.on('data', chunk => {
        body += chunk.toString();
      });
      req.on('end', () => {
        const visitorData = JSON.parse(body);
        const visitor = {
          id: visitors.length + 1,
          visitor: visitorData.visitor
        }
        visitors.push(visitor);
        res.end(JSON.stringify(visitor));
      });
    } else {
      res.statusCode = 404;
      res.end("Page not found");
    }
  });

server.listen(port, hostname, () => {
  console.log(`Server running at http://${hostname}:${port}/`);
});


